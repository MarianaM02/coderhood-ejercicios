/*
## 2. Stock agotado
Dado el siguiente array de productos en stock, agregar a cada producto un atributo booleano **agotado** en true si el stock es mayor a 0, caso contrario false.

const productos = [
    { id: "1", nombre: "lapiceras", stock: 164 },
    { id: "2", nombre: "marcadores", stock: 0 },
    { id: "3", nombre: "cartulinas", stock: 25 },
    { id: "4", nombre: "cartucheras", stock: 0 },
    { id: "5", nombre: "mochilas", stock: 4 }
]

resultado esperando
[
    { id: "1", nombre: "lapiceras", stock: 164, agotado: false },
    { id: "2", nombre: "marcadores", stock: 0, agotado: true },
    { id: "3", nombre: "cartulinas", stock: 25, agotado: false },
    { id: "4", nombre: "cartucheras", stock: 0, agotado: true },
    { id: "5", nombre: "mochilas", stock: 4, agotado: false }
]
*/

function estaAgotado(stock){
    return stock === 0;
}

function agregarAgotado(elemento){
    return {
        ...elemento,
        agotado: elemento.stock===0
    }
}


const productos = [
    { id: "1", nombre: "lapiceras", stock: 164 },
    { id: "2", nombre: "marcadores", stock: 0 },
    { id: "3", nombre: "cartulinas", stock: 25 },
    { id: "4", nombre: "cartucheras", stock: 0 },
    { id: "5", nombre: "mochilas", stock: 4 }
]

// console.log(agregarAgotado(productos))
const productosConAgotado = productos.map(agregarAgotado);
console.log(productosConAgotado);




// El método filter() crea un nuevo array (subarray) con todos los elementos que cumplan la condición implementada.
// El método map() crea un nuevo array con los resultados de la llamada a la función indicada aplicados a cada uno de sus elementos.

// El método filter() crea un nuevo array (subarray) con todos los elementos que cumplan la condición implementada.
// El método map() sirve para "transformar" cada elemento del array en otra cosa, dando como resultado un nuevo array con todos los elementos transformados. El método map no modifica el array original, sino que devuelve uno nuevo.