/*
## Ejercicio 1 - a
Reescribir la siguiente función tal que retorne una promesa que se cumpla luego de 3 segundos (usar setTimeout), devolviendo el mismo resultado que la función dada.

function dividirNumeros(dividendo, divisor) {
    return dividendo / divisor;
}*/

// setInterval()


function dividirNumeros(dividendo, divisor) {
    return new Promise ((resolve, reject) => {
        try {
            setTimeout(() => {
                resolve(dividendo / divisor)
            }, 2000)
        }catch(error){
            reject(error)
        }
    })
}




/*
## Ejercicio 1 - b
Usando **then** y **catch** completar la función "programa" dada para llamar a **dividirNumeros** e imprimir el resultado o imprimir un mensaje de error si ocurriera. Para provocar un error pueden enviar 0 como divisor.
*/
function callbackDeExito(resultadoFinal){
    console.log(`El resultado obtenido es: ${resultadoFinal}`);
} 
function callbackDeFallo(errorOcurrido){
    console.log(`Ocurrio un error: ${errorOcurrido}`);
} 
function programa() {
    // Tu código acá
    const promesa = dividirNumeros(6,2);
    promesa.then(callbackDeExito).catch(callbackDeFallo);
}

programa();

/*
## Ejercicio 1 - c
Reescribir la función **programa** del insiso anterior para que use **async** y **await**
*/

async function programa2() {
    // Tu código acá
    try {
        const resultado = await dividirNumeros(10,2);
    } catch (error) {
        console.log(`Ocurrio un error: ${error}`);
    }
}

programa2();
