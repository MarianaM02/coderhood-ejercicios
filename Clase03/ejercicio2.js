// 2. Hacer una función que reciba una serie de palabras separadas por espacios y que imprimala misma frase pero en orden inverso.

function fraseReversa(frase){
    let fraseVector= frase.split(" "); 
    // fraseVector.reverse();
    let nuevaFrase = fraseVector.reverse().join(" ");
    console.log(nuevaFrase);

}

fraseReversa("Hacer una función que reciba una serie de palabras separadas por espacios");