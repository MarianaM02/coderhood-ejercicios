/**
 - Crear una funcion 'randomError' que tenga 50% de posibilidades de disparar un error con mensaje "Error catastrofico"
 - En caso de no disparar un error, la funcion debe devolver un mensaje "Todo ok"
 - Capturar el error e informar 'Error capturado'
**/

function randomError(){
    if(Math.random()<0.5){
        //Esto hace que se ejecute un 50% de las veces
        throw new Error("ERROR! 😱");
    }
    return "Todo OK 😊"
}

try {
    //ambiente seguro
    let resultado = randomError();
    console.log(resultado);
} catch (error) {
    console.log(error.message);
}