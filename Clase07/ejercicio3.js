/*
### 3) Descarga de informacion prohibida
Crear una funcion llamada `pedirUsuario` que retorna un usuario al azar de la siguiente lista:

let lista = [{
    id: 19310,
    nombre: "Bautista",
},{
    id: 90010,
    nombre: "Ema",
},{
    id: 00519,
    nombre: "Lucas",
},{
    id: 00000,
    nombre: "Meison",
}
]

La funcion `pedirUsuario` disparar un error llamado 'ForbiddenInformation' si el usuario elegido es el del `id` = 00000
Llamar a la funcion `pedirUsuario` dentro de la funcion `pedirInformacion` para cambiar el retorno de la funcion `pedirInformacion`.
Considerar que la funcion `pedirInformacion` ahora tiene que capturar un error, cuando la capture debe disparar un error 'NetworkError'.
 */

function pedirUsuario(){
    const lista = [{
        id: 19310,
        nombre: "Bautista"
    },{
        id: 90010,
        nombre: "Ema"
    },
    {
        id: 0051, 
        nombre: "Lucas"
    },{
        id: 00000,
        nombre: "Meison"
    }
    ]
    const indice = Math.floor(Math.random()*lista.length);
    if(lista[indice].id === 00000){
        throw new Error("NetworkError");
    }
    return lista[indice];
}

function pedirInformacion(){
    try {
        const usuario = pedirUsuario();
        return usuario;
    } catch (error) {
        console.log(error.message);
    }
}

console.log(pedirInformacion());