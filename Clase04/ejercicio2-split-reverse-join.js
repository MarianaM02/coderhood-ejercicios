/**Hacer una función que reciba una serie de palabras separadas por espacios y que imprima la misma frase pero en orden inverso. */

function invertirFrase(frase){
    const vector = frase.split(" ");
    const fraseInvertida = vector.reverse().join(" ");
    return fraseInvertida;
}

console.log(invertirFrase("You can set the length property to truncate an array at any time."))