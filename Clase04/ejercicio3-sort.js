/**Hacer una función que reciba como parámetro tres números enteros y que lo ordene de mayor a menor. */

function compare(a, b) {
    if (a < b) {
        return -1;
    } else if (a > b) {
        return 1;
    } else {
        return 0;
    }
}

function ordernarDeMayorAMenor(num1, num2, num3) {
  let vector=[num1, num2, num3];
  const ordenado=vector.sort(compare).reverse();
  return ordenado.toString();
}

console.log(ordernarDeMayorAMenor(634,546,1234));
