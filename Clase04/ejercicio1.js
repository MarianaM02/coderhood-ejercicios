/**Hacer una función que reciba un numero y que retorne verdadero si es par o falso en caso contrario. */
function esPar(numero){
    return numero%2 === 0;
}
console.log(esPar(34));
console.log(esPar(5));
console.log(esPar(3));
console.log(esPar(4));