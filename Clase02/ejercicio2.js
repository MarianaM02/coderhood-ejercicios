/*2)*Contador*

Sea una variable numérica entera y positiva 'limite':
-Recorrer desde 0 hasta limite
-Imprimir al final del programa la cantidad de números impares
-Imprimir al final del programa la cantidad de números menores de limite / 2*/

let limite2=20;

console.log("Numeros impares:")
for (let i = 0; i < limite2; i++) {
    if (i%2!=0) {
        console.log(i);
    }
}

console.log("Numeros menores de limite/2:")
for (let i = 0; i < limite2/2; i++) {
    console.log(i);
}