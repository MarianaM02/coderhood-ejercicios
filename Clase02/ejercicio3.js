/*3)Contador 2: La venganza del contador
Sea una variable numérica entera y positiva 'limite':
-Recorrer desde 0 hasta que se cumpla una de las siguientes condiciones:
---Se llega a limite
---La cantidad de números pares desde 0 hasta limite es mayor a una variable anteriormente creada llamada final
-Al final del recorrido imprimir la cantidad de números múltiplos de 3
-Al final del recorrido imprimir la suma de todos los números entre el 0 hasta que se termine el recorrido*/

let limite3 = 14;
let finalizacion = 8;
let contador = 0;
let cantPares = 0;
let multiplosDe3 = 0;
let sumatoria = 0;

while (contador != limite3 && cantPares < finalizacion) {
  if (contador % 2 === 0) {
    cantPares++;
  }

  if (contador % 3 === 0) {
    multiplosDe3++;
  }

  sumatoria += contador;

  contador++;
}

console.log("Números múltiplos de 3: ", multiplosDe3);
console.log("Sumatoria: ", sumatoria);
