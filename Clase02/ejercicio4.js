/*4)*La secuencia de Fibonacci*

En matemáticas, la sucesión o serie de Fibonacci es la siguiente sucesión infinita de números naturales:0 , 1 , 1 , 2 , 3 , 5 , 8 , 13 , 21 
La sucesión comienza con los números 0 y 1,  a partir de estos cada término es la suma de sus dos anteriores
Sea una variable numérica entera y positiva 'limite':
-Recorrer desde 0 hasta limite
-Imprimir tantos términos de la secuencia como repeticiones de 0 hasta limite
Ejemplo: si limite = 6, imprimir 0, 1, 1, 2, 3, 5*/

function fibonacci(limite){
    let a=0;
    let b=1;
    let aux;
    
    console.log(a);
    console.log(b);
    
    do{
        console.log(b);

        aux=b;
        b=a+b;
        a=aux;

    }while (b<limite)

}

fibonacci(40);