/*1)*Pares*

Sea una variable numérica entera y positiva 'limite':
- Recorrer desde 0 hasta `limite`

- Imprimir en pantalla los números pares (No la cantidad de números)*/

let limite1=7;

console.log("Numeros pares:")
for (let i = 0; i < limite1; i++) {
    if (i%2===0) {
        console.log(i);
    }
}
